-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2015 at 05:46 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `treeslife_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ms_user`
--

CREATE TABLE IF NOT EXISTS `ms_user` (
  `id_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `photo` varchar(100) NOT NULL DEFAULT 'resource/default_photo.jpg',
  `id_role` tinyint(4) NOT NULL DEFAULT '1',
  `last_login` timestamp NULL DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ms_user`
--

INSERT INTO `ms_user` (`id_user`, `username`, `password`, `email`, `name`, `photo`, `id_role`, `last_login`, `create_date`) VALUES
(1, 'luthfielaroeha', '5f4dcc3b5aa765d61d8327deb882cf99', 'luthfielaroeha@gmail.com', 'Muhamad Luthfie La Roeha', 'resource/default_photo.jpg', 1, '2015-05-03 11:07:35', '2015-05-03 09:34:00'),
(2, 'wvhiegt', '5f4dcc3b5aa765d61d8327deb882cf99', 'wvhiegt@gmail.com', NULL, 'resource/default_photo.jpg', 1, '2015-05-03 11:46:56', '2015-05-03 11:46:56');

-- --------------------------------------------------------

--
-- Table structure for table `ref_role`
--

CREATE TABLE IF NOT EXISTS `ref_role` (
  `id_role` tinyint(4) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `role_desc` text,
  `create_date` timestamp NULL DEFAULT NULL,
  `expired_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ref_role`
--

INSERT INTO `ref_role` (`id_role`, `role_name`, `role_desc`, `create_date`, `expired_date`) VALUES
(1, 'User', 'Default user', '2015-05-03 15:45:25', '2015-05-03 15:45:25');
