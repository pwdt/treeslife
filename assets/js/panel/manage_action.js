$(function(){
    $lat = ($('#us3-lat').val()==0)?-7.276841:$('#us3-lat').val();
    $lon = ($('#us3-lon').val()==0)?112.79164600000001:$('#us3-lon').val();
    $('#us3').locationpicker({
        location: {latitude:$lat , longitude: $lon},
        radius: 1,
        inputBinding: {
            latitudeInput: $('#us3-lat'),
            longitudeInput: $('#us3-lon'),
            locationNameInput: $('#us3-address')
        },
        enableAutocomplete: true,
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            // Uncomment line below to show alert on each Location Changed event
            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
        }
    }); 
});