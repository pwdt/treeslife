$(function(){
	$(".form-group .fi-inline").on('focusin', function(){
		$(this).addClass('fi-focus');
	});
	$(".form-group .fi-inline").on('focusout', function(){
		$(this).removeClass('fi-focus');
	});

	$("form a.submit-btn").on('click', function(){
		$(this).closest('form').submit();
	});
});