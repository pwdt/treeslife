<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function signin()
	{
		$data['username'] = "";
		if ($this->input->post())
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$this->load->model('user_model');
			if (($details = $this->user_model->sign_in($username, $password)) !== FALSE)
			{
				$this->session->set_userdata('id_user', $details->id_user);
				$this->session->set_userdata('id_role', $details->id_role);
				
				$user = $this->user_model->details($details->id_user);
				if ($user->id_role == 2)
				{
					redirect('user');
				}
				else
				{
					redirect('panel');
				}
				
			}
			else
			{
				$data['username'] = $username;
				$this->session->set_flashdata('information', 'Wrong username or password !');
			}
		}

		$data['view'] = 'guest/sign_in';
		$this->load->view('templates/front', $data); 
	}

	public function signup()
	{
		$data = array(
			'username'	=> '',
			'email'		=> ''
		);
		if ($this->input->post())
		{
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$fullname = $this->input->post('fullname');

			$this->load->model('user_model');
			if ($this->user_model->check_username($username) !== FALSE)
			{
				$register_data = array(
					'username'	=> $username,
					'email' => $email,
					'password' => md5($password),
					'fullname' => $fullname
				);
				$user_id = $this->user_model->register($username, $email, $password,2,$fullname);

				$this->session->set_userdata('id_user', $user_id);
				redirect('user');
			}
			else
			{
				$data['username'] = $username;
				$this->session->set_flashdata('Username exist !');
			}
		}

		$data['view'] = 'guest/sign_up';
		$this->load->view('templates/front', $data);
	}


	public function donate_anonymous()
	{
		$this->load->model('donation_model');
		if ($this->input->post())
		{
			$amount = $this->input->post('InputAmount');
			$user_id = $this->donation_model->insert(0,1, $amount);
			redirect('Home');
		}
	}

}
