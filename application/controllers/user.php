<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	private $user_id;

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
        $this->load->model('donation_model');
		if ($this->session->has_userdata('id_user') === FALSE)
			redirect('pages/signin');
		else
			$this->user_id = $this->session->userdata('id_user');
	}

	public function index()
	{
		$data['user_details'] = $this->user_model->details($this->user_id);
		$data['sum'] = $this->donation_model->checksumuser($this->user_id);
        $data['a']['b'] = "HALO";
		$data['css_plugin']  = array(
			'calendar/calendar.css'
		);

		$data['js_plugin']  = array(
			'calendar/calendar.js'
		);

		$data['subview'] = 'user/home';
		$data['view'] = 'user/template';
		$this->load->view('user/front', $data);
	}

	public function complete_profile()
	{
		echo "Complete profile here";
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('');
	}
    
    public function donate($id)
	{
		if ($this->input->post())
		{
			$amount = $this->input->post('InputAmount');
			$this->load->model('action_model');
			$action = $this->action_model->detailsactive()->id_action;
			$user_id = $this->donation_model->insert($id, $action, $amount);
			redirect('user');
		}
	}
    
    public function feedback($id)
    {
        if ($this->input->post())
		{
            $this->load->model('feedback_model');
			$title = $this->input->post('title');
			$description = $this->input->post('description');
            $user_id = $this->feedback_model->register($id, $title, $description);
			redirect('user');
		}
	}
}
