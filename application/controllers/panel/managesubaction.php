<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Managesubaction extends CI_Controller 
{
    private $user_id;
    function __construct()
	{
		parent::__construct();
		$this->load->model('subaction_model');

		if ($this->session->has_userdata('id_user') === FALSE)
			redirect('pages/signin');
		else
			$this->user_id = $this->session->userdata('id_user');
        
	}
    
	public function index()
	{
        $this->load->model('user_model');
        $data['user_details'] = $this->user_model->details($this->user_id);
		$data['view'] = 'panel/template';
        $data['content'] = 'panel/subaction_admin';
        $data['table'] = $this->subaction_model->listall();  
		$this->load->view('templates/panel', $data);
	}
    
    public function add_subaction()
    {
        $data = array(
			'name'	=> '',
			'description'		=> ''
		);
		if ($this->input->post())
        {
            $idact = $this->input->post('idact');
            $order =$this->input->post('order');
			$name = $this->input->post('name');
			$description = $this->input->post('description');
			$threshold = $this->input->post('threshold');
            if ($this->input->post('status') == NULL)
            $status = 0;
            else
            $status = $this->input->post('status');
			$this->load->model('subaction_model');
			if ($this->subaction_model->check_name($name) !== FALSE)
			{
				$this->subaction_model->register($idact, $order, $name, $description, $threshold, $status);
				redirect('panel/managesubaction');
			}
			else
			{
				$data['name'] = $name;
				$this->session->set_flashdata('Name exist !');
			}
        }
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/add_subaction';  
		$this->load->view('templates/panel', $data);
    }   
    
    public function edit_subaction($id)
    {
        	if ($this->input->post())
		    {
                
                    $idact = $this->input->post('idact');
                    $order =$this->input->post('order');
                    $name = $this->input->post('name');
                    $description = $this->input->post('description');
                    $threshold = $this->input->post('threshold');
                    if ($this->input->post('status') == NULL)
                    $status = 0;
                    else
                    $status = $this->input->post('status');
                    $success = $this->input->post('success');
                    $successdate = $this->input->post('successdate');
                    $this->load->model('subaction_model');
                    $edit_data = array(
                        'id_action'     => $idact,
                        'order_index'   => $order,
                        'action_name'   => $name,
                        'action_desc'	=> $description,
                        'threshold'		=> $threshold,
                        'active_status' => $status,
                        'is_success'    => $success,
                        'success_date'  => $successdate
                    );
            $this->subaction_model->update_data($id, $edit_data);

				redirect('panel/managesubaction');
		    }
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/edit_subaction';
        $data['table'] = $this->subaction_model->details($id);  
		$this->load->view('templates/panel', $data);
	   
    }
    
    public function delete_subaction($id)
    {
        $this->subaction_model->delete($id);
        redirect('panel/managesubaction');
    }

}
