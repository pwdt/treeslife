<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	private $user_id;
	function __construct()
	{
		parent::__construct();

		if ($this->session->has_userdata('id_user') === FALSE)
			redirect('pages/signin');
		else
			$this->user_id = $this->session->userdata('id_user');
	}

	public function index()
	{
        $this->load->model('action_model');
        $this->load->model('subaction_model');
		$this->load->model('donation_model');
		$this->load->model('user_model');
		$data['user_details'] = $this->user_model->details($this->user_id);
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/dashboard';
        $data['table'] = $this->action_model->detailsactive();
        $data['sum'] = $this->donation_model->checksum($data['table']->id_action); 
        $data['action'] = $this->subaction_model->detailsby($data['table']->id_action); 
		$this->load->view('templates/panel', $data);
	}

		
	
}
