<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manageaction extends CI_Controller 
{
    private $user_id;
    function __construct()
	{
		parent::__construct();
		$this->load->model('action_model');

		if ($this->session->has_userdata('id_user') === FALSE)
			redirect('pages/signin');
		else
			$this->user_id = $this->session->userdata('id_user');
        
	}
    
	public function index()
	{
        $this->load->model('user_model');
        $data['user_details'] = $this->user_model->details($this->user_id);
		$data['view'] = 'panel/template';
        $data['content'] = 'panel/action_admin';
        $data['table'] = $this->action_model->listall();  
		$this->load->view('templates/panel', $data);
	}
    
    public function add_action()
    {

        $data = array(
			'name'	=> '',
			'description'		=> ''
		);
		if ($this->input->post())
		{
			$name = $this->input->post('name');
			$description = $this->input->post('description');
			$threshold = $this->input->post('threshold');
            $due = $this->input->post('due');
            $latitude = $this->input->post('latitude');
            $longitude = $this->input->post('longitude');
			$this->load->model('action_model');
			if ($this->action_model->check_name($name) !== FALSE)
			{
				$this->action_model->register($name, $description, $threshold, $due, $latitude, $longitude);
				redirect('panel/manageaction');
			}
			else
			{
				$data['name'] = $name;
				$this->session->set_flashdata('Name exist !');
			}
		}
        $data['js_plugin'] = array(
            'gmaps/gmaps.api.js',
            'gmaps/locationpicker.jquery.min.js',
            '../js/panel/manage_action.js'
        );
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/add_action';  
		$this->load->view('templates/panel', $data);
        
    }
    
    public function edit_action($id)
    {
    	if ($this->input->post())
	    {
            
                $description = $this->input->post('description');
                $threshold = $this->input->post('threshold');
                $due = $this->input->post('due');
                $status = $this->input->post('status');
                $latitude = $this->input->post('latitude');
                $longitude = $this->input->post('longitude');
                $edit_data = array(
                    'description'	=> $description,
                    'threshold'		=> $threshold,
                    'due_date'      => $due,
                    'latitude'      => $latitude,
                    'longitude'     => $longitude,
                    'action_status' => $status
                );
        $this->action_model->update_data($id, $edit_data);

			redirect('panel/manageaction');
	    }

        $data['js_plugin'] = array(
            'gmaps/gmaps.api.js',
            'gmaps/locationpicker.jquery.min.js',
            '../js/panel/manage_action.js'
        );

        $data['view'] = 'panel/template';
        $data['content'] = 'panel/edit_action';
        $data['table'] = $this->action_model->details($id);  
		$this->load->view('templates/panel', $data);
	   
    }
    
    public function delete_action($id)
    {
        $this->action_model->delete($id);
        redirect('panel/manageaction');
    }

}
