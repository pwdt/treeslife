<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller 
{
	private $user_id;
    function __construct()
	{
		parent::__construct();
		$this->load->model('feedback_model');

		if ($this->session->has_userdata('id_user') === FALSE)
			redirect('pages/signin');
		else
			$this->user_id = $this->session->userdata('id_user');
	}
    
	public function index()
	{
		$this->load->model('user_model');
		$data['user_details'] = $this->user_model->details($this->user_id);
		$data['view'] = 'panel/template';
        $data['content'] = 'panel/feedback';
        $data['table'] = $this->feedback_model->listall();  
		$this->load->view('templates/panel', $data);
	}
	
}
