<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends CI_Controller 
{
    private $user_id;
    function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');

		if ($this->session->has_userdata('id_user') === FALSE)
			redirect('pages/signin');
		else
			$this->user_id = $this->session->userdata('id_user');
	}
    
	public function index()
	{
        $this->load->model('user_model');
        $data['user_details'] = $this->user_model->details($this->user_id);
		$data['view'] = 'panel/template';
        $data['content'] = 'panel/user_admin';
        $data['table'] = $this->user_model->listall();  
		$this->load->view('templates/panel', $data);
	}
    
    public function add_user()
    {
        $data = array(
			'username'	=> '',
			'email'		=> ''
		);
		if ($this->input->post())
		{
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$nama = $this->input->post('nama');
            $role = $this->input->post('role');
            $password = $this->input->post('password');
            
			$this->load->model('user_model');
			if ($this->user_model->check_username($username) !== FALSE)
			{
				$this->user_model->register($username, $email, $password, $role, $nama);
				redirect('panel/manage');
			}
			else
			{
				$data['username'] = $username;
				$this->session->set_flashdata('Username exist !');
			}
		}
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/add_user';  
		$this->load->view('templates/panel', $data);
        
    }
    
    public function edit_user($id)
    {
        	if ($this->input->post())
		    {
                if ($this->input->post('password'))
                {
                    $email = $this->input->post('email');
                    $nama = $this->input->post('nama');
                    $role = $this->input->post('role');
                    $password = $this->input->post('password');
                    $edit_data = array(
                        'email'			=> $email,
                        'password'		=> md5($password),
                        'name'          => $nama,
                        'id_role'       => $role
                    );
            $this->user_model->update_data($id, $edit_data);

				redirect('panel/manage');
		    }
            else
            {
                $username = $this->input->post('username');
                    $email = $this->input->post('email');
                    $nama = $this->input->post('nama');
                    $role = $this->input->post('role');
                    $edit_data = array(
                        'email'			=> $email,
                        'name'          => $nama,
                        'id_role'       => $role
                    );
            $this->user_model->update_data($id, $edit_data);
            redirect('panel/manage');
            }
        }
        $data['view'] = 'panel/template';
        $data['content'] = 'panel/edit_user';
        $data['table'] = $this->user_model->details($id);  
		$this->load->view('templates/panel', $data);
	   
    }
    
    public function delete_user($id)
    {
        $this->user_model->delete($id);
        redirect('panel/manage');
    }

}
