<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $this->load->model('action_model');
        $this->load->model('subaction_model');
		$this->load->model('donation_model');
        $data['table'] = $this->action_model->detailsactive();
        $data['sum'] = $this->donation_model->checksum($data['table']->id_action); 
        $data['action'] = $this->subaction_model->detailsby($data['table']->id_action); 
		$this->load->view('guest/front', $data);
	}

	public function contributors()
	{
		$this->load->model('action_model');

		$data['action'] = $this->action_model->get_successful_action();

		$this->load->view('guest/contributors', $data);
	}
}
