<?php

class Action_model extends CI_Model
{
	private $table = "ms_action";

	function __construct()
	{
		parent::__construct();
	}


	function register($name, $description, $threshold, $due, $latitude, $longitude)
	{
		$register_data = array(
			'name'		    => $name,
			'description'	=> $description,
			'threshold'		=> $threshold,
            'due_date'      => $due,
            'latitude'      => $latitude,
            'longitude'     => $longitude,
			'create_date'	=> date('Y-m-d H:i:s')
		);
		
		$this->db->insert($this->table, $register_data);

		return $this->db->insert_id();
	}
	
	function get_successful_action()
	{
		$now = date('Y-m-d');
		$this->db->where("due_date <= '{$now}'");
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function details($id_action)
	{
		$this->db->where('id_action', $id_action);
		$query = $this->db->get($this->table);

		return $query->row();
	}
    
    function detailsactive()
	{
		$this->db->where('action_status', 1);
		$query = $this->db->get($this->table);

		return $query->row();
	}
    
    function listall()
    {
        $query = $this->db->get($this->table);
        
        return $query->result();
    }

	function update_data($id, $array)
	{
		$this->db->where('id_action', $id);
		$this->db->update($this->table, $array);
	}
	
	function delete($user_id)
	{
		$this->db->where('id_action', $user_id);
		$this->db->delete($this->table);
		
		return TRUE;
	}
	
	function check_name($name)
	{
		$name = strtolower($name);
		$this->db->where('name', $name);
		
		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0)
			return FALSE;
		else 
			return TRUE;
	}

}