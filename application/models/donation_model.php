<?php

class Donation_model extends CI_Model
{
    private $table = "tr_donation";

    function __construct()
    {
        parent::__construct();
    }

    function details($id_user)
    {
        $this->db->select ('username,mf.*');
        $this->db->where('id_user', $id_user);
        $this->db->join('ms_user mu', 'mu.id_user = mf.id_user');
        $query = $this->db->get($this->table." mf");

        return $query->row();
    }
    
    function listall()
    {
        $this->db->select ('username,mf.*');
        $this->db->join('ms_user mu', 'mu.id_user = mf.id_user');
        $query = $this->db->get($this->table." mf");
        
        return $query->result();
    }
    
    function checksum($id)
    {
        $query = $this->db->select_sum('donation_amount', 'Amount');
        $query = $this->db->where('id_action', $id);
        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result->Amount;
    }
    
    function checksumuser($id)
    {
        $query = $this->db->select_sum('donation_amount', 'Amount');
        $query = $this->db->where('id_user', $id);
        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result->Amount;
    }
        
    function insert($id_user,$id_action,$donation_amount)
    {
        $register_data = array(
            'id_user'             => $id_user,
            'id_action'           => $id_action,
            'donation_amount'     => $donation_amount,
            'donate_time'         => date("Y-m-d H:i:s")
        );
        $this->db->insert($this->table, $register_data);
        return $this->db->insert_id();
    }

    function get_action()
    {
        $this->db->select ('username,mf.*');
        $this->db->join('ms_user mu', 'mu.id_user = mf.id_user');
        $query = $this->db->get($this->table." mf");
        
        return $query->result();
    }   

    
}