<?php

class Feedback_model extends CI_Model
{
	private $table = "ms_feedback";

	function __construct()
	{
		parent::__construct();
	}

	function register($id, $title, $description)
	{
		$register_data = array(
			'id_user'   => $id,
			'title'			=> $title,
			'description'	=> $description,
			'create_date'	=> date('Y-m-d H:i:s'),
		);
		
		$this->db->insert($this->table, $register_data);

		return $this->db->insert_id();
	}
	
	function details($id_user)
	{
		$this->db->select ('username,mf.*');
		$this->db->where('id_user', $id_user);
		$this->db->join('ms_user mu', 'mu.id_user = mf.id_user');
		$query = $this->db->get($this->table." mf");

		return $query->row();
	}
    
    function listall()
    {
    	$this->db->select ('username,mf.*');
		$this->db->join('ms_user mu', 'mu.id_user = mf.id_user');
		$query = $this->db->get($this->table." mf");
        
        return $query->result();
    }

	function update_data($id, $array)
	{
		$this->db->where('id_user', $id);
		$this->db->update($this->table, $array);
	}
	
	function delete($user_id)
	{
		$this->db->where('id_user', $user_id);
		$this->db->delete($this->table);
		
		return TRUE;
	}
	
	function check_username($username)
	{
		$username = strtolower($username);
		$this->db->where('username', $username);
		
		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0)
			return FALSE;
		else 
			return TRUE;
	}
}