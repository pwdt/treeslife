<?php

class Subaction_model extends CI_Model
{
	private $table = "ms_sub_action";

	function __construct()
	{
		parent::__construct();
	}


	function register($idact, $order, $name, $description, $threshold, $status)
	{
		$register_data = array(
			'id_action'     => $idact,
            'order_index'   => $order,
            'action_name'   => $name,
			'action_desc'   => $description,
			'threshold'		=> $threshold,
            'active_status' => $status,
			'create_date'	=> date('Y-m-d H:i:s')
		);
		
		$this->db->insert($this->table, $register_data);

		return $this->db->insert_id();
	}
	
	function details($id_subaction)
	{
		$this->db->where('id_sub_action', $id_subaction);
		$query = $this->db->get($this->table);

		return $query->row();
	}
    
    function detailsby($id)
	{
		$this->db->where('id_action', $id);
		$query = $this->db->get($this->table);

		return $query->result();
	}
    
    function listall()
    {
        $query = $this->db->get($this->table);
        
        return $query->result();
    }

	function update_data($id, $array)
	{
		$this->db->where('id_sub_action', $id);
		$this->db->update($this->table, $array);
	}
	
	function delete($user_id)
	{
		$this->db->where('id_sub_action', $user_id);
		$this->db->delete($this->table);
		
		return TRUE;
	}
	
	function check_name($name)
	{
		$name = strtolower($name);
		$this->db->where('action_name', $name);
		
		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0)
			return FALSE;
		else 
			return TRUE;
	}
}
?>