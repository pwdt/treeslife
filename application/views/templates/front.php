<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?php echo base_url(); ?>"></base>
    <title>TreesLife</title>

    <!-- Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/material-design-iconic-font.min.css" rel="stylesheet">
    

    <!-- Vendor CSS -->
    <?php if(isset($css_plugin)): ?>
    <?php foreach($css_plugin as $css_val):?>
    <link href="assets/vendors/<?php echo $css_val?>" rel="stylesheet" />
    <?php endforeach;?>
    <?php endif;?>
        
    <!-- CSS -->
    <link href="assets/css/global.css" rel="stylesheet">
    <link href="assets/css/front.css" rel="stylesheet">
    
    <script type="text/javascript">
        $base_url = "<?php echo base_url()?>";
    </script>

    <!-- Javascript Libraries -->
    <script src="assets/js/jquery-2.1.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
    <!-- Vendor JS -->
    <?php if(isset($js_plugin)): ?>
    <?php foreach($js_plugin as $js_val):?>
    <script src="assets/vendors/<?php echo $js_val?>"></script>
    <?php endforeach;?>
    <?php endif;?>

    <!-- JS -->
    <script src="assets/js/global.js"></script>
    <script src="assets/js/front.js"></script>
    
</head>
<body>
    <?php
    if (isset($subdata))
        $this->load->vars($subdata);
    ?>
    <?php $this->load->view($view); ?>

</body>
</html>