
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add User
                        </h1>
                        
                    </div>
                </div>
                <!-- /.row -->

                
                <div class="row">
                <form class="form-horizontal" method="post" action="index.php/panel/manage/add_user">
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">User</label>
                    <div class="col-sm-10">
                    <input type="text" name="username" class="form-control" id="inputEmail3" placeholder="Username">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-lg-1 col-md-2 control-label">Password</label>
                    <div class="col-sm-10">
                    <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Nama</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="nama" id="inputEmail3" placeholder="Nama">
                    </div>
                </div>
                   <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Email</label>
                    <div class="col-sm-10">
                    <input type="email" class="form-control" name="email" id="inputEmail3" placeholder="Email">
                    </div>
                </div>
                   <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">User</label>
                    <div class="col-sm-10">
                    <select name="role" class="form-control">
                        <option value="1">Admin</option>
                        <option value="2">User</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-11 text-center">
                        <button type="submit" class="btn btn-default ">Register</button>
                    </div>
                </div>
                </form>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
