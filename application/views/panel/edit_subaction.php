
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit Sub-Action
                        </h1>
                        
                    </div>
                </div>
                <!-- /.row -->

                
                <div class="row">
                <form class="form-horizontal" method="post" action="index.php/panel/managesubaction/edit_subaction/<?php echo $table->id_sub_action ?>">
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">ID Order</label>
                    <div class="col-sm-10">
                    <input type="number" name="idact" class="form-control" id="inputEmail3" placeholder="ID Action" value="<?php echo $table->id_action?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Order Index</label>
                    <div class="col-sm-10">
                    <input type="number" name="order" class="form-control" id="inputEmail3" placeholder="Order Index" value="<?php echo $table->order_index?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Name</label>
                    <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Name" value="<?php echo $table->action_name?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Description</label>
                    <div class="col-sm-10">
                    <input type="text" name="description" class="form-control" id="inputEmail3" placeholder="Description" value="<?php echo $table->action_desc?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Threshold</label>
                    <div class="col-sm-10">
                    <input type="number" name="threshold" class="form-control" id="inputEmail3" placeholder="Threshold" value="<?php echo $table->threshold?>"/>
                    </div>
                </div>    
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Active</label>
                    <div class="col-sm-10">
                    <input type="checkbox" name="status" class="form-control" id="inputEmail3" placeholder="Status" value="1" <?php if($table->active_status == 1) echo "checked"; ?>/>
                    </div>
                </div>    
                  <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Success</label>
                    <div class="col-sm-10">
                    <input type="text" name="success" class="form-control" id="inputEmail3" placeholder="Success" value="<?php echo $table->is_success?>"/>
                    </div>
                  </div>      
                  <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Active</label>
                    <div class="col-sm-10">
                    <input type="datetime" name="successdate" class="form-control" id="inputEmail3" placeholder="Success Date" value="<?php echo $table->success_date?>"/>
                    </div>
                  </div>      
                <div class="form-group">
                    <div class="col-sm-11 text-center">
                        <button type="submit" class="btn btn-default ">Submit</button>
                    </div>
                </div>
                </form>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
