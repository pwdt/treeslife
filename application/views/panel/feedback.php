            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Feedback
                        </h1>
                        
                    </div>
                </div>
                <!-- /.row -->

                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Search</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Username</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Time</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         <?php
                                            $x=1;
                                            foreach($table as $row):
                                            ?>
                                            <tr>
                                                <td><?php echo $x++;?></td>
                                                <td><?php echo $row->username?></td>
                                                <td><?php echo $row->title?></td>
                                                <td>
                                                    <a href="#" class="btn btn-primary btn-block shadow" data-toggle="modal" data-target="#details">Details</a>
                                                        <div class="modal fade" id="details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                          <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Description</h4>
                                                              </div>
                                                              <div class="modal-body">
                                                                <?php echo $row->description?>
                                                              </div>
                                                              <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                </td>
                                                <td><?php echo date('d F Y, H:i:s',strtotime($row->create_date))?></td>
                                                <td><?php echo $row->is_read?></td>
                                            </tr>
                                            <?php 
                                            endforeach;
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
