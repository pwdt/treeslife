
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Sub-Action
                        </h1>
                        
                    </div>
                </div>
                <!-- /.row -->

                
                <div class="row">
                <form class="form-horizontal" method="post" action="index.php/panel/managesubaction/add_subaction">
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">ID Order</label>
                    <div class="col-sm-10">
                    <input type="number" name="idact" class="form-control" id="inputEmail3" placeholder="ID Action">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Order Index</label>
                    <div class="col-sm-10">
                    <input type="number" name="order" class="form-control" id="inputEmail3" placeholder="Order Index">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Name</label>
                    <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Description</label>
                    <div class="col-sm-10">
                    <input type="text" name="description" class="form-control" id="inputEmail3" placeholder="Description">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Threshold</label>
                    <div class="col-sm-10">
                    <input type="number" name="threshold" class="form-control" id="inputEmail3" placeholder="Threshold">
                    </div>
                </div>    
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Active</label>
                    <div class="col-sm-10">
                    <input type="checkbox" name="status" class="form-control" id="inputEmail3" placeholder="Status" value="1">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-11 text-center">
                        <button type="submit" class="btn btn-default ">Submit</button>
                    </div>
                </div>
                </form>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
