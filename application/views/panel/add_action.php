
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Action
                        </h1>
                        
                    </div>
                </div>
                <!-- /.row -->

                
                <div class="row">
                <form class="form-horizontal" method="post" action="index.php/panel/manageaction/add_action">
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Name</label>
                    <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Description</label>
                    <div class="col-sm-10">
                    <input type="text" name="description" class="form-control" id="inputEmail3" placeholder="Description">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Threshold</label>
                    <div class="col-sm-10">
                    <input type="number" name="threshold" class="form-control" id="inputEmail3" placeholder="Threshold">
                    </div>
                </div>    
                <div class="form-group">
                    <label for="inputEmail3" class="col-lg-1 col-md-2 control-label">Due Date</label>
                    <div class="col-sm-10">
                    <input type="date" name="due" class="form-control" id="inputEmail3" placeholder="Due Date">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Location:</label>

                    <div class="col-sm-10"><input type="text" class="form-control" id="us3-address"/></div>
                </div>
                <div class="form-group text-center">
                <div class="col-md-offset-3" id="us3" style="width: 550px; height: 400px;"></div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-sm-11 text-center">
                        <input type="hidden" class="form-control" style="width: 110px" id="us3-lon" name="longitude" />
                        <input type="hidden" class="form-control" style="width: 110px" id="us3-lat" name="latitude" />
                        <button type="submit" class="btn btn-default ">Submit</button>
                    </div>
                </div>
                </form>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
