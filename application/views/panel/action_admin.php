
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Manage Action
                        </h1>
                        
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <a class="btn btn-login btn-primary btn-float waves-effect waves-button submit-btn" href="index.php/panel/manageaction/add_action"><i class="md md-person-add"></i></a>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Transactions Panel</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Threshold</th>
                                                <th>Status</th>
                                                <th>Due Date</th>
                                                <th>Latitude</th>
                                                <th>Longitude</th>
                                                <th>Update</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $x=1;
                                            foreach($table as $row):
                                            ?>
                                            <tr>
                                                <td><?php echo $x++;?></td>
                                                <td><?php echo $row->name?></td>
                                                <td><?php echo $row->description?></td>
                                                <td><?php echo $row->threshold?></td>
                                                <td><?php echo $row->action_status?></td>
                                                <td><?php echo $row->due_date?></td>
                                                <td><?php echo $row->latitude?></td>
                                                <td><?php echo $row->longitude?></td>
                                                <td><a href="index.php/panel/manageaction/edit_action/<?php echo $row->id_action ?>">Update</a> </td>
                                                <td><a href="index.php/panel/manageaction/delete_action/<?php echo $row->id_action ?>">Delete</a> </td>
                                            </tr>
                                            <?php 
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
