
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Manage User
                        </h1>
                        
                    </div>
                </div>
                <!-- /.row -->

                
                <div class="row">
                    <a class="btn btn-login btn-primary btn-float waves-effect waves-button submit-btn" href"panel/manage/add_user"><i class="md md-person-add"></i></a>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Transactions Panel</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Nama</th>
                                                <th>Role</th>
                                                <th>Last Login</th>
                                                <th>Update</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $x=1;
                                            foreach($table as $row):
                                            ?>
                                            <tr>
                                                <td><?php echo $x++;?></td>
                                                <td><?php echo $row->username?></td>
                                                <td><?php echo $row->email?></td>
                                                <td><?php echo $row->name?></td>
                                                <td><?php echo $row->id_role?></td>
                                                <td><?php echo $row->last_login?></td>
                                                <td><a href="index.php/panel/manage/edit_user/<?php echo $row->id_user ?>">Update</a> </td>
                                                <td><a href="index.php/panel/manage/delete_user/<?php echo $row->id_user ?>">Delete</a> </td>
                                            </tr>
                                            <?php 
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
