<div class="login-pages">
	<div class="login-header">
	</div>
	<div class="login-content">
		<form action="index.php/pages/signin" method="post">
			<div class="form-group">
		        <label class="fi-label fi-icon"><i class="md md-person"></i></label>
		        <div class="fi-inline">
		            <input name="username" type="text" class="form-control" placeholder="Username" autofocus value="<?php echo $username ?>" />
		        </div>
		    </div>
			<div class="form-group">
		        <label class="fi-label fi-icon"><i class="md md-lock"></i></label>
		        <div class="fi-inline">
		            <input name="password" type="password" class="form-control" placeholder="Password">
		        </div>
		    </div>
		    <input type="submit" class="hidden" />
		    <a href="javascript:;" class="btn btn-login btn-primary btn-float waves-effect waves-button submit-btn"><i class="md md-arrow-forward"></i></a>
		</form>
		<div class="login-nav">
			<a class="login-nav-btn" href="index.php/pages/signup">Register</a>
            <a class="login-nav-btn" href="#" style="background-color: #F44336;">Home</a>            
		</div>
		<?php if($this->session->flashdata('information') != ""): ?>
			<?php echo $this->session->flashdata('information'); ?>
		<?php endif; ?>
	</div>

</div>	