<!DOCTYPE html>
<html>
  <head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <base href="<?php echo base_url(); ?>"></base>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/global.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/front.css" rel="stylesheet">
    <link href="assets/css/material-design-iconic-font.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="icon" href="assets/images/logo/favicon.png" sizes="32x32">
    <script src="assets/js/jquery-2.1.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <title>TreesLife</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
    <script src="assets/js/global.js"></script>
    <script src="assets/js/front.js"></script>
    <script src="assets/vendors/nicescroll/jquery.nicescroll.min.js"> </script>
    <script>
function initialize() {
  var myLatlng = new google.maps.LatLng(-12.363882,120.044922);
  var mapOptions = {
    zoom: 4,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  <?php foreach($action as $row): ?>
  var myLatlng = new google.maps.LatLng(<?php echo $row->latitude; ?>,<?php echo $row->longitude; ?>);
  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: ''
  });
  <?php endforeach; ?>
}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>
  </head>
  <body>
    <nav class="scrolled navbar navbar-custom navbar-fixed-top" role="navigation">

        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img class="img-logo" src="assets/images/logo/treeslife.png" /> TreesLife</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="custom-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#intro">Home</a></li>
                    <li><a href="#today">Situation</a></li>
                    <li><a href="#actionT">Action</a></li>
                    <li><a href="#donate-login">Donate</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#login">Login</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#register">Register</a></li>
                </ul>
            </div>

        </div>

    </nav>
    
    <div id="map-canvas"></div>
  </body>
</html>