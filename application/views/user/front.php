<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <base href="<?php echo base_url(); ?>"></base>
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Treeslife User Page</title>
    <!-- BOOTSTRAP STYLE SHEET -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- FONT-AWESOME STYLE SHEET FOR BEAUTIFUL ICONS -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/material-design-iconic-font.min.css" rel="stylesheet">
     <!-- CUSTOM STYLE CSS -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/global.css" rel="stylesheet">
    <link rel="icon" href="assets/images/logo/favicon.png" sizes="32x32">
    <style type="text/css">
               .bttn-social {
            color: white;
            opacity: 0.8;
        }

            .bttn-social:hover {
                color: white;
                opacity: 1;
                text-decoration: none;
            }

        .btn-facebook {
            background-color: #3b5998;
        }

        .btn-twitter {
            background-color: #00aced;
        }

        .btn-linkedin {
            background-color: #0e76a8;
        }

        .btn-google {
            background-color: #c32f10;
        }
        .navbar-custom .nav li a:hover,
        .navbar-custom .nav li a:focus,
        .navbar-custom .nav li.active {
            background: #388E3C;
            font-weight:400;
        }
        .navbar-custom .navbar-nav > li > a,
        .navbar-custom .navbar-nav .dropdown-menu > li > a {
	       color: #000;
	       font-size: 14px;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <nav class="navbar-custom navbar-fixed-top" role="navigation" style="background-color: #4CAF50; padding: 0; box-shadow: 0 1px 4px rgba(0, 0, 0, .26);">

                <div class="container" style="background-color: #4CAF50;">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><img class="img-logo" src="assets/images/logo/treeslife circle.png" /> TreesLife</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="custom-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#intro">Home</a></li>
                            <li><a href="#today">Situation</a></li>
                            <li><a href="#actionT">Action</a></li>
                            <li><a href="#donate-login">Donate</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="md md-person"></i> <?php echo $user_details->username?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="javascript:;" data-toggle="modal" data-target="#cprofile"><i class="md md-mail"></i> Change Profile</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-toggle="modal" data-target="#cpassword"><i class="md md-settings"></i> Change Password</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#"><i class="md md-settings-power"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </div>

            </nav>
    <!-- NAVBAR CODE END -->
        <div class="container" style="margin-top: 3%;">
            <section style="padding-bottom: 50px; padding-top: 50px;">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4" style="margin-bottom: 3%;">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" style="background-color: #795548; border-color: #5D4037; color: #fff;">
                        <div class="alert" sytle="height:auto ; width:100%;">
                            <h2>Welcome, <?php echo $user_details->name?></h2>
                            <h4>Last Login: <?php echo $user_details->last_login?></h4>
                            <h4>Total Donate: <?php echo $sum?></h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-5">
                        <h2>Donate now</h2>
                        <form class="form-inline" action="index.php/user/donate/<?php echo $user_details->id_user?>" method="post">
                            <div class="form-group" style="margin-top: 5%;">
                                <label class="sr-only" for="exampleInputAmount">Amount (in rupiah)</label>
                                <div class="input-group">
                                      <div class="input-group-addon">Rp</div>
                                        <input type="text" class="form-control" name="InputAmount" placeholder="Amount">
                                      <div class="input-group-addon">.00</div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <button type="submit" class="btn btn-primary">Transfer cash</button>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form action="index.php/user/feedback/<?php echo $user_details->id_user?>" method="post">
                        <div class="form-group col-md-8">
                            <h3>Feedback to us for improvement</h3>
                            <input type="text" class="form-control" name="title" placeholder="Title">
                            <br>
                            <textarea class="form-control" rows="2" name="description" placeholder="Description"></textarea>
                            
                            <br>
                            <button type="submit" class="btn btn-primary">Send Feedback</a>
                        </div>
                        </form>    
                    </div>
                </div>
                <!-- ROW END -->
                        
            </section>
            <!-- SECTION END -->
            <div class="modal fade" id="cprofile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header" style="background-color: #5cb85c;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="color: rgba(255, 255, 255, 0.87); text-align: center;" id="myModalLabel">Change Profile</h4>
                  </div>   
                  <div class="modal-body">
                        <label>Registered Name</label>
                        <input type="text" class="form-control" placeholder="Jhon Deo">
                        <label>Registered Email</label>
                        <input type="text" class="form-control" placeholder="jnondeao@gmail.com">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Update Details</button>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="modal fade" id="cpassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header" style="background-color: #5cb85c;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="color: rgba(255, 255, 255, 0.87); text-align: center;" id="myModalLabel">Change Your Password</h4>
                  </div>
                  <div class="modal-body">
                        <label>Enter Old Password</label>
                        <input type="password" class="form-control">
                        <label>Enter New Password</label>
                        <input type="password" class="form-control">
                        <label>Confirm New Password</label>
                        <input type="password" class="form-control" />
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Change Password</button>
                  </div>
                </div>
              </div>
            </div>
        
        <!-- CONATINER END -->
    </div>
        
    <footer class="text-center">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-4">
                            <h3>Location</h3>
                            <p>
                            KBS, Jl Suterejo Selatan VI no 2<br>
                            Surabaya, Jawa Timur, Indonesia 90210
                            </p>
                        </div>
                        <div class="footer-col col-md-4">
                            <h3>Around the Web</h3>
                            <ul class="list-inline">
                                <li>
                                    <a href="//www.facebook.com/SaintGraveyard" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook" style="margin-top: 23%"></i></a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/+JulioAnthonyLeonards/posts" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"style="margin-top: 23%"></i></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/imjuanleonard" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"style="margin-top: 23%"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-col col-md-4">
                            <h3>About Treeslife</h3>
                            <p>
                                Treeslife is a company that move in Green Action <br>
                                Our sole purpose is only for saving the world <br>
                                We can't close our eyes anymore at this deprecated world <br> 
                                So, Are you the one of us? <br>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            Copyright &copy; WP Dream Team<br/>
                            Part of KBS Project<br/>
                            2015
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    <!-- REQUIRED SCRIPTS FILES -->
    <!-- CORE JQUERY FILE -->
    <script src="assets/js/jquery-2.1.3.min.js"></script>
    <!-- REQUIRED BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>
